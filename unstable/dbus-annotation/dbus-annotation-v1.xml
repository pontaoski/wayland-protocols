<?xml version="1.0" encoding="UTF-8"?>
<protocol name="dbus_annotation_manager_unstable_v1">
  <copyright><![CDATA[
    Copyright © 2017 David Edmundson
    Copyrihgt © 2020 Carson Black

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice (including the next
    paragraph) shall be included in all copies or substantial portions of the
    Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
  ]]></copyright>

  <description summary="Wayland protocol for associating DBus objects with toplevels">
    This description provides a high-level overview of the interplay between
    the interfaces defined in this protocol. For details, see the protocol
    specification.

    The dbus_annotation_manager allows a client to request the creation of an appmenu
    object associated with an xdg_toplevel. The appmenu object allows a client to
    notify the compositor of a DBus com.canonical.dbusmenu object associated with
    itself.

    Clients should request the creation of an dbus_annotation object when they create a
    DBus object associated with an xdg_toplevel, and should release the object when they
    destroy a DBus object associated with their xdg_toplevel.

    Clients should only own at most one dbus_annotation object with a given name for each of their xdg_toplevel
    objects. A protocol error will be raised if a client requests more than one dbus_annotation
    object for an xdg_toplevel with a given name.
  </description>

  <interface name="zwp_dbus_annotation_manager_v1" version="1">
    <description summary="controller object for registering dbus objects associated with xdg_toplevels">
      An object that provides access to the creation of dbus_annotation objects.
    </description>

    <request name="destroy" type="destructor">
      <description summary="release the memory for the application menu manager object">
        Destroy the wp_dbus_annotation_manager object. wp_dbus_annotation objects created from this
        object remain valid and should be destroyed separately.
      </description>
    </request>

    <request name="create">
      <arg name="name" type="string" />
      <arg name="id" type="new_id" interface="zwp_dbus_annotation_v1"/>
      <arg name="toplevel" type="object" interface="xdg_toplevel"/>
    </request>

    <enum name="error">
      <entry name="role" value="0" summary="given xdg_toplevel already has a dbus_annotation with the same name"/>
    </enum>
  </interface>

  <interface name="zwp_dbus_annotation_unstable_v1" version="1">
    <description summary="controller object for associating dbus objects with an xdg_toplevel">
      An object that provides access to clients to notify the compositor of
      associated DBus objects for an xdg_toplevel.

      If not applicable, clients should remove this object.
    </description>

    <request name="destroy" type="destructor">
      <description summary="release the dbus annotation object"/>
    </request>

    <request name="set_address">
      <description summary="notify the compositor of a dbus object">
        Set or update the service name and object path corresponding to the
        DBus object. The DBus object should be registered on the session bus
        before sending this request.

        Strings should be formatted in Latin-1 matching the relevant DBus
        specifications.
      </description>
      <arg name="service_name" type="string" summary="the dbus service name of a dbus object"/>
      <arg name="object_path" type="string" summary="the dbus object path of a dbus object"/>
    </request>
  </interface>
</protocol>
